<?php

namespace CloudTowerDev\EasyPanelServer;

use YunTaIDC\Plugin\PluginBase;

use YunTaIDC\Events\CreateServiceEvent;
use YunTaIDC\Events\RenewServiceEvent;
use YunTaIDC\Events\DeleteserviceEvent;
use YunTaIDC\Events\LoginServiceEvent;
use YunTaIDC\Events\ProductConfigEvent;

class EasyPanelServer extends PluginBase{

    public function SendData($array, $url){
        $result = file_get_contents($url. http_build_query($array));
        return json_decode($result, true);
    }
    
    public function CreateService(CreateServiceEvent $event){
        $Product = $event->getProduct();
        $Server = $Product->getServer();
        if(empty($Server->getServerIp())){
            $this->getSystem()->getLogger()->addSystemLog('服务'.$event->getService()->getUsername().'开通失败，服务器IP为空');
            return false;
        }else{
            $Configoption = $Product->getConfigOption();
            if(empty($Configoption['product_id'])){
                $this->getSystem()->getLogger()->addSystemLog('服务'.$event->getService()->getUsername().'开通失败，插件配置product_id为空');
                return false;
            }else{
                // exit($Server->getServerAccessHash());
                $rand = mt_rand(1000, 9999);
                $params = array(
                    'a' => 'add_vh',
                    'c' => 'whm',
                    'r' => $rand,
                    's' => md5('add_vh'.$Server->getServerAccessHash().$rand),
                    'init' => 1,
                    'name' => $event->getService()->getUsername(),
                    'passwd' => $event->getService()->getPassword(),
                    'product_id' => $Configoption['product_id'],
                    'json' => 1,
                );
                $url = 'http://'.$Server->getServerIp().':'.$Server->getServerPort().'/api/index.php?';
                $result = $this->SendData($params, $url);
                if($result['result'] == 200){
                    return true;
                }else{
                    $this->getSystem()->getLogger()->addSystemLog('服务'.$event->getService()->getUsername().'开通失败，返回结果'.$result['result']);
                    return false;
                }
            }
        }
    }
    
    public function RenewService(RenewServiceEvent $event){
        return true;
    }
    
    public function DeleteService(DeleteServiceEvent $event){
        $Server = $event->getServer();
        if(empty($Server->getServerIp())){
            $this->getSystem()->getLogger()->addSystemLog('服务'.$event->getService()->getUsername().'删除失败，服务器IP为空');
            return false;
        }else{
            $rand = mt_rand(1000, 9999);
            $params = array(
                'a' => 'del_vh',
                'c' => 'whm',
                'r' => $rand,
                's' => md5('del_vh'.$Server->getServerAccessHash().$rand),
                'name' => $event->getService()->getUsername(),
                'json' => 1,
            );
            $url = 'http://'.$Server->getServerIp().':'.$Server->getServerPort().'/api/index.php?';
            $result = $this->SendData($params, $url);
            if($result['result'] == 200){
                return true;
            }else{
                $this->getSystem()->getLogger()->addSystemLog('服务'.$event->getService()->getUsername().'删除失败，返回结果'.$result['result']);
                return false;
            }
        }
    }
    
    public function LoginService(LoginServiceEvent $event){
        echo '<form action="http://'.$event->getServer()->getServerIp().':'.$event->getServer()->getServerPort().'/vhost/index.php?c=session&a=login" method="POST"><input type="hidden" name="username" value="'.strtolower($event->getService()->getUsername()).'"><input type="hidden" name="passwd" value="'.$event->getService()->getPassword().'"><button type="submit">点击登陆</button></form>';
    }
    
    public function ProductConfig(ProductConfigEvent $event){
        return array(
            'product_id' => array('type'=>'number', 'label'=>'easypanel里的产品ID', 'placeholder'=>'easypanel里的产品ID'),
        );
    }
    
}

?>